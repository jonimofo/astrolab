---
title: "Library"
subtitle: ""
# meta description
description: "This is meta description"
draft: false
layout: "about"


# about
about:
  title: "Library"
  content: "Create a best strategic tool, share it with your team and ensure it’s on track with intuitive dashboards. Simple enough with the flexibility Lorem ipsum dolor sit amet consectetur adipisicing elit. "
  image: "images/about.jpg"

---